Source: aufs
Section: kernel
Priority: optional
Maintainer: Jan Luca Naumann <j.naumann@fu-berlin.de>
Build-Depends: debhelper (>=12), dkms, linux-kbuild-5.7
Standards-Version: 4.4.0
Homepage: http://aufs.sourceforge.net/
Vcs-Browser: https://salsa.debian.org/janluca-guest/aufs-debian
Vcs-Git: https://salsa.debian.org/janluca-guest/aufs-debian.git

Package: aufs-dkms
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Recommends: aufs-tools
Suggests: aufs-dev
Description: DKMS files to build and install aufs
 The aufs driver provides a union mount for Linux filesystems. It allows one
 to virtually merge the contents of several directories and/or stack them, so
 that file changes in the aufs union mount end up as changes in only one
 of the source directories.
 .
 This package contains the DKMS files to build and install aufs.
 .
 To install this package, you have to install the header files for your current
 kernel manually (usually included in the "linux-headers-<your architecture>").

Package: aufs-dev
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Recommends: aufs-dkms
Replaces: linux-libc-dev (<< 3.18-1~exp1)
Breaks: linux-libc-dev (<< 3.18-1~exp1)
Description: Development files for aufs
 The aufs driver provides a union mount for Linux filesystems. It allows one
 to virtually merge the contents of several directories and/or stack them, so
 that file changes in the aufs union mount end up as changes in only one
 of the source directories.
 .
 This package contains the development files for aufs.
